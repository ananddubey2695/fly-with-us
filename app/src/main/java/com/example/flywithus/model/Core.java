package com.example.flywithus.model;

import com.google.gson.Gson;

import java.io.Serializable;

/**
 * Created by anand on 2019-05-05 at 14:36.
 */
public abstract class Core implements Serializable {
    @Override
    public String toString() {
        Gson g = new Gson();
        return g.toJson(this);
    }
}
