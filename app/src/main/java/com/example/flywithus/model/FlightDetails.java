package com.example.flywithus.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ananddubey
 * Dated- 2019-06-06
 */
public class FlightDetails extends Core {

    String id;

    String origin;

    String destination;

    @SerializedName("originmap")
    String originMap;

    @SerializedName("destinationmap")
    String destinationMap;

    String status;

    String scheduleTime;

    String terminal;

    String checkInDesk;

    @SerializedName("gates")
    String gate;

    @SerializedName("airlineName")
    String flightName;

    @SerializedName("airlineCode")
    String flightCode;
}
