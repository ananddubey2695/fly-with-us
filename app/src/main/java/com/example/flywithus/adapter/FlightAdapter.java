package com.example.flywithus.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.flywithus.R;
import com.example.flywithus.model.FlightDetails;

import java.util.ArrayList;
import java.util.List;

public class FlightAdapter extends RecyclerView.Adapter<FlightAdapter.ScanViewHolder> {

    private List<FlightDetails> flights = new ArrayList<>();

    public FlightAdapter(List<FlightDetails> flights) {
        this.flights.addAll(flights);
    }

    @NonNull
    @Override
    public ScanViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        return new ScanViewHolder(inflater.inflate(R.layout.item_list_flight,
                parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final ScanViewHolder scanViewHolder, int position) {

    }

    @Override
    public int getItemCount() {
        return flights.size();
    }

    void updateData(List<FlightDetails> flightDetailsList) {
        if (flights != null && flightDetailsList != null && !flightDetailsList.isEmpty()) {
            if (!flights.isEmpty())
                flights.clear();
            flights.addAll(flightDetailsList);
            notifyDataSetChanged();
        }
    }

    static class ScanViewHolder extends RecyclerView.ViewHolder {

        ScanViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
