package com.example.flywithus.feature.flightlist;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.example.flywithus.model.FlightDetails;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by ananddubey
 * Dated- 2019-06-06
 */
public class FlightListViewModel extends ViewModel {
    private FlightListModel model;

    MutableLiveData<List<FlightDetails>> flightsLiveData;

    public FlightListViewModel() {
        model = new FlightListModel();
    }

    void getFlightList() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.HOUR_OF_DAY, 2);
        String startTime = String.valueOf(calendar.getTimeInMillis());
        calendar.add(Calendar.HOUR_OF_DAY, -4);
        String endTime = String.valueOf(calendar.getTimeInMillis());

        model.fetchFlights(startTime, endTime)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<List<FlightDetails>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onSuccess(List<FlightDetails> flightList) {
                        flightsLiveData.postValue(flightList);
                    }

                    @Override
                    public void onError(Throwable e) {
                    }
                });
    }

    public MutableLiveData<List<FlightDetails>> getFlightsLiveData() {
        if (flightsLiveData == null)
            flightsLiveData = new MutableLiveData<>();
        return flightsLiveData;
    }
}
