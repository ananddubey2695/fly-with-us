package com.example.flywithus.feature.flightlist;

import android.arch.lifecycle.Observer;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.example.flywithus.R;
import com.example.flywithus.adapter.FlightAdapter;
import com.example.flywithus.model.FlightDetails;

import java.util.Collections;
import java.util.List;

public class FlightListActivity extends AppCompatActivity {

    FlightListViewModel viewModel;
    RecyclerView mFlightList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flight_list);

        mFlightList = findViewById(R.id.flight_list);

        FlightAdapter adapter = new FlightAdapter(Collections.emptyList());

        getViewModel().getFlightsLiveData()
                .observe(this, flightDetails -> {
                    if (flightDetails != null && !flightDetails.isEmpty()) {
                        mFlightList.setVisibility(View.VISIBLE);
                    } else {
                        mFlightList.setVisibility(View.GONE);
                    }
                });

        viewModel.getFlightList();
    }

    private FlightListViewModel getViewModel() {
        if (viewModel == null)
            viewModel = new FlightListViewModel();
        return viewModel;
    }
}
