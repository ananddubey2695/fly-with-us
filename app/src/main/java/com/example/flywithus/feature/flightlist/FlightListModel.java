package com.example.flywithus.feature.flightlist;

import com.example.flywithus.model.FlightDetails;
import com.example.flywithus.network.NetworkManager;

import java.util.List;

import io.reactivex.Single;

/**
 * Created by ananddubey
 * Dated- 2019-06-06
 */
public class FlightListModel {
    Single<List<FlightDetails>> fetchFlights(String startTime, String endTime) {
        return NetworkManager.getInstance()
                .getFlightApi()
                .getFlights("cgk", startTime, endTime)
                .map(flightResponse -> {
//                    if (flightResponse.success != null && flightResponse.success)
                    return flightResponse.flightList;
                });
    }
}
