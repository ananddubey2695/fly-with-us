package com.example.flywithus.network.response;

import com.example.flywithus.model.Core;
import com.example.flywithus.model.FlightDetails;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ananddubey
 * Dated- 2019-06-06
 */
public class FlightResponseWrapper extends Core {
    public Boolean success;

    @SerializedName("flights")
    public List<FlightDetails> flightList;

    @SerializedName("querystarttime")
    public String queryStartTime;

    @SerializedName("queryendtime")
    public String queryEndTime;
}