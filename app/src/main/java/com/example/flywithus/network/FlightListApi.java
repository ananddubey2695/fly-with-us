package com.example.flywithus.network;

import com.example.flywithus.network.response.FlightResponseWrapper;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface FlightListApi {

    @GET("/flightsroute/getflightfeed")
    public Single<FlightResponseWrapper> getFlights(@Query("appname") String appName,
                                                    @Query("starttime") String startTime,
                                                    @Query("endtime") String endTime);
}
